import wikipediaapi
import random
import requests
import time
from tqdm import tqdm
import sys
import re
import numpy as np
import copy
import os
import pickle
import json

from math import log10, floor
def round_sig(x, n):
    try:
        return round(x, -int(floor(log10(abs(x))))+(n-1))
    except ValueError:
        return 0.0

import requests
from bs4 import BeautifulSoup

class TermStat():

    def __init__(self, term, freq, nt=None, degree=None):
        self.term = term
        self.freq = freq
        if nt is None:
            self.nt = 1
        else:
            self.nt = nt
        if degree is None:
            self.degree = len(term.split())
        else:
            self.degree = degree

    def __repr__(self):
        return f'TermStat("{self.term}", {self.freq}, nt={self.nt}, degree={self.degree})'

    def __add__(self, other):
        if not isinstance(other, TermStat):
            raise TypeError(f'TermStat can only be added to other TermStat. Got `{type(other)}` instead.')
        if self.term != other.term:
            raise ValueError(f'Cannot combine stats for two distinct terms: `{self.term}` and `{other.term}`')
        new_freq = self.freq + other.freq
        new_nt = self.nt + other.nt
        return TermStat(self.term, new_freq, new_nt, self.degree)

    def __sub__(self, other):
        if self.term != other.term:
            raise ValueError(f'Cannot subtract different terms: `{self.term}` and `{other.term}`')
        return TermStat(self.term, self.freq - other.freq, 0, self.degree)

class TextStat():

    def __init__(self, text=None, d=None):
        if text is None:
            self.ngrams = {}
        elif isinstance(text, str):
            self.ngrams = self._get_ngrams(text)
        elif isinstance(text, dict):
            self.ngrams = text
        else:
            raise TypeError(f'Got unrecognized type `{type(text)}` for initializing TextStat. Expected `str` or `dict`')
        if d is None:
            if text is None:
                self.d = 0
            else:
                self.d = 1
        else:
            self.d = d

    def __add__(self, other):
        if isinstance(other, WikipediaText):
            return self + other.text_stat
        # See WikipediaRacer.add_page(); it is (large dict) + (small dict), only loop other's keys.
        new_d = self.d + other.d

        new_ts = {**self.ngrams}
        for term, ts in other.ngrams.items():
            if term in new_ts.keys():
                new_ts[term] = new_ts[term] + ts
            else:
                new_ts[term] = ts
        return TextStat(new_ts, new_d)

    def __sub__(self, other):
        return TextStat({
            term: self[term] - (other[term] if term in other.ngrams.keys() else TermStat(term, 0, 0))
        for term,ts in self.ngrams.items()})

    def __mul__(self, other):
        return TextStat({ term: TermStat(term, ts.freq*other, ts.nt, ts.degree) for term, ts in self.ngrams.items()}, self.d)

    def __getitem__(self, term):
        return self.ngrams[term]

    def _flatten(self, inlist):
        return [ l for li in inlist for l in li ]

    def _get_ngrams(self, text):
        done = False
        ngrams = {}

        word_lists = [ sentence.split() for sentence in text.split('. ')]

        # Parse out ngrams per degree
        start = 1
        end = 3 # float('inf')

        itr = start

        while not done:
            term_list = self._flatten([(lambda wl: [ ' '.join([ wl[j] for j in range(i,i+itr)]) for i in range(len(wl)-itr+1) ])(word_list) for word_list in word_lists])
            done = (len(term_list) == len(set(term_list)) and end == float('inf')) or itr == end+1
            if not done:
                for term in term_list:
                    if term in ngrams.keys():
                        ngrams[term] += 1
                    else:
                        ngrams[term] = 1
            itr += 1

        ngrams = { term: TermStat(term, freq) for term, freq in ngrams.items() }
        return ngrams

class WikipediaText():

    def __init__(self, page, text_stat=None):
        if isinstance(page, wikipediaapi.WikipediaPage):
            self.text = self._clean_text(page)
        elif isinstance(page, str):
            # This is to apply to titles
            self.text = page
        if text_stat is None:
            self.text_stat = TextStat(self.text)
        else:
            self.text_stat = text_stat

    def __add__(self, other):
        # reduce to addition of TextStat
        if isinstance(other, WikipediaText):
            return self.text_stat + other.text_stat
        if isinstance(other, TextStat):
            return self.text_stat + other
        raise TypeError(f'Addition of `WikipediaText` not supported with type: {type(other)}')

    def __sub__(self, other):
        # Reduce to subtraction between TextStat
        return self.text_stat - other.text_stat

    def __mul__(self, other):
        # Reduce to multiplication of TextStat
        return WikipediaText(self.text, self.text_stat * other)

    def _flatten(self, inlist):
        return [ l for li in inlist for l in li ]

    def _split_it(self, text):
        """
        Splits text after sentences, parenthesis, commas, spaces, etc.
        """
        text_list = text.split(', ')
        text_list = self._flatten([ t.split('(') for t in text_list ])
        text_list = self._flatten([ t.split(')') for t in text_list ])
        text_list = self._flatten([ t.split('[') for t in text_list ])
        text_list = self._flatten([ t.split(']') for t in text_list ])
        text_list = self._flatten([ t.split('{') for t in text_list ])
        text_list = self._flatten([ t.split('}') for t in text_list ])
        text_list = self._flatten([ t.split('"') for t in text_list ])
        text_list = self._flatten([ t.split(" '") for t in text_list ])
        text_list = self._flatten([ t.split("' ") for t in text_list ])
        text_list = self._flatten([ t.split(" - ") for t in text_list ])
        text_list = self._flatten([ t.split(" — ") for t in text_list ])
        text_list = self._flatten([ t.split(" – ") for t in text_list ])
        text_list = self._flatten([ t.split(";") for t in text_list ])
        text_list = self._flatten([ t.split(":") for t in text_list ])
        text_list = self._flatten([ t.split("==") for t in text_list ])
        text_list = self._flatten([ t.split() for t in text_list ])
        return text_list

    def _add_section_texts(self, section, texts):
        for s in section.sections:
            self._add_section_texts(s, texts)
        texts.append(section.text)

    def _get_relevant_text(self, page):
        """
        Strips some sections of the text, to only get relevant text.
        """
        bad_sections = [
            'See also',
            'References',
            'Bibliography',
            'Further reading'
        ]
        texts = None
        while texts is None:
            try:
                texts = [page.summary]
            except (requests.exceptions.ReadTimeout, requests.exceptions.ConnectionError):
                time.sleep(random.random()*10)
            except json.decoder.JSONDecodeError:
                return ''
            except KeyError:
                try:
                    print(f'KeyError for: {page.title}')
                except:
                    pass
                return ''
        secs = page.sections
        for s in secs:
            if s.title not in bad_sections:
                self._add_section_texts(s, texts)
        return ' '.join(texts).lower()

    def _clean_text(self, page):
        text = self._get_relevant_text(page)
        text = self._split_it(text)
        return ' '.join(text)

class WikipediaRacer():

    def __init__(self):

        if not os.path.isdir(os.environ['HOME'] + '/.wikiracer'):
            os.mkdir(os.environ['HOME'] + '/.wikiracer')

        self.storage_file = os.environ['HOME'] + '/.wikiracer/wiki.pickle'

        if os.path.isfile(self.storage_file):
            with open(self.storage_file, 'rb') as f:
                self.wiki, self.terms, self.pages = pickle.load(f)
        else:
            self.wiki = wikipediaapi.Wikipedia('en')
            self.terms = TextStat()
            self.pages = {}

        self.target_title = None
        self.target_text = None

    def __len__(self):
        return len(self.pages)

    def random_title(self, safe=False):
        title = '?:'
        while self._is_special(title):
            r = requests.get('https://en.wikipedia.org/wiki/Special:Random')
            title = re.sub(' - Wikipedia','',BeautifulSoup(r.text, 'lxml').find('title').contents[0])
            if safe:
                p = self.get_page(title)
                links = self._clean_link_titles(p.links.keys())
                backlinks = self._clean_link_titles(p.backlinks.keys())
                if len(links) == 0 or len(backlinks) == 0:
                    title = '?:'

        return title

    def _is_special(self, link_title):
        # perhaps have 'List of' entries?
        if 'Template talk:' in link_title or\
            'Template:' in link_title or\
            'Portal:' in link_title or\
            'Category:' in link_title or\
            'Book:' in link_title or\
            'Help:' in link_title or\
            'Module:' in link_title or\
            'Wikipedia:' in link_title or\
            'File:' in link_title or\
            '?:' in link_title or\
            'User:' in link_title or\
            'User talk:' in link_title or\
            '(identifier)' in link_title or\
            'List of ' in link_title or\
            'Wikipedia Talk:' in link_title or\
            'Talk:' in link_title:
            return True
        return False

    def _clean_link_titles(self, link_titles):
        return [
            t
        for t in link_titles if not self._is_special(t)]

    def fitness(self, target_title, current_title, nw=0.5, ip=4.0, dw=(0.1,1.0,10.0,100.0,1000.0),lp=1.0,tp=2.0, fitness=True):
        if target_title != self.target_title:

            if target_title not in self.pages.keys() and not isinstance(current_title, WikipediaText):
                self.add_page(target_title)

            if isinstance(target_title, WikipediaText):
                target_text = target_title
            else:
                target_text = WikipediaText(self.pages[target_title])

        else:
            target_text = self.target_text

        if current_title not in self.pages.keys() and not isinstance(current_title, WikipediaText):
            self.add_page(current_title)

        if isinstance(current_title, WikipediaText):
            current_text = current_title
        else:
            current_text = WikipediaText(self.pages[current_title])

        div = len(current_text.text.split())
        if div == 0:
            div = 1
        text_ratio_scale = (len(target_text.text.split())/div)

        text_diff = target_text - current_text * text_ratio_scale**lp

        if fitness:
            fit = 0
        else:
            fit = {}
        for term,ts in text_diff.ngrams.items():
            # If the freq is negative, it means there is an overcount of that term,
            # in current, which is better than an undercount, since at least the term
            # is present.
            # The IDF is the weight of the relevance of that term; IDF = 0 if the term
            # exists in all documents, and larger the more rare it is.
            # Then we can have a weight depending on degree. Just a word (degree=1) may
            # not be too important, but pairs or larger may be more telling ragarding
            # context.
            # Then it may also be important to account for text-length ratio. A long text
            # may have more matching frequencies just due to the text being longer. So if
            # the text is longer, we may want to scale up the fitness due to this.
            term_rel = 2*self.terms.d**2/(self.terms.d**2 + self.terms[term].nt**2) - 1 # np.log(self.terms.d/self.terms[term].nt)
            f = (ts.freq if ts.freq > 0 else nw*abs(ts.freq))**tp * \
                    float(term_rel**ip) * \
                    dw[min(len(dw)-1,ts.degree-1)]
            if fitness:
                fit += f
            else:
                fit[term] = f

        return fit

    def print_top(self, target_title, num=10):
        fit_dict = self.fitness(target_title, WikipediaText(''), fitness=False)
        tot = sum(fit_dict.values())
        max_degree, min_degree = (lambda x: (max(x), min(x)))([ len(term.split()) for term in fit_dict.keys() ])
        tops = [ sorted([ (term, round_sig(fit, 2), round_sig(fit/tot*100, 2)) for term, fit in fit_dict.items() if len(term.split()) == d ], key=lambda x: x[2], reverse=True)[:num] for d in range(min_degree, max_degree+1) ]
        max_terms = [ max(map(len, [ term for term, fit, prc in tops[i]])) for i in range(len(tops)) ]
        tops = list(map(list, zip(*tops)))

        print()

        s = ''
        for l in max_terms:
            s += '{:<' + str(l) + '} | {:<8} | {:<8} || '
        s[:-4]
        s = s.format(*[ t for _ in range(len(max_terms)) for t in ('term', 'fit', '%') ])
        print(s)
        print('-'*len(s))

        for r in range(len(tops)):
            s = ''
            for d in range(len(tops[r])):
                s += '{:<' + str(max_terms[d]) + '} | {:<8} | {:<8} || '
                s = s.format(*tops[r][d])
            s[:-4]
            print(s)

        print()

    def get_page(self, title):
        if title in self.pages.keys():
            return self.pages[title]
        else:
            self.add_page(title)
            return self.pages[title]

    def add_page(self, title):
        # add as page
        if not isinstance(title, str):
            raise TypeError(f'Provided title is not a string, got "{type(title)}" instead.')
        self.pages[title] = self.wiki.page(title)
        # add terms
        self.terms = self.terms + WikipediaText(self.pages[title])

    def add_random_page(self, safe=False):
        self.add_page(self.random_title(safe=safe))

    def add_random_pages(self, num=10, safe=False):
        for _ in tqdm(range(num), 'Adding random pages', file=sys.__stdout__):
            self.add_random_page(safe=safe)

    def get_link_title_list(self, title, fn=lambda x: random.random()):
        if title not in self.pages.keys():
            self.add_page(title)
        links = None
        while links is None:
            try:
                links = self.pages[title].links.items()
            except (requests.exceptions.ReadTimeout, requests.exceptions.ConnectionError):
                time.sleep(random.random()*10)
            except json.decoder.JSONDecodeError:
                links = []
        if len(links) == 0:
            return links
        link_titles, _ = zip(*links)
        link_titles = self._clean_link_titles(link_titles)
        link_titles = list(set(link_titles))
        # This sorting can take a few seconds if a fitness-based function is used
        link_titles = sorted(link_titles, key=fn)
        return link_titles

    def get_backlink_title_list(self, title, sort=False):
        if title not in self.pages.keys():
            self.add_page(title)
        links = self.pages[title].backlinks.items()
        link_titles, _ = zip(*links)
        link_titles = self._clean_link_titles(link_titles)
        link_titles = list(set(link_titles))
        if sort:
            link_titles = sorted(link_titles, key=lambda x: len(self.get_page(x).backlinks), reverse=True)
        return link_titles

    def set_target(self, target_title):
        if target_title not in self.pages.keys():
            self.add_page(target_title)
        if self.target_title is None:
            print(f'Setting target to: {target_title}')
        else:
            print(f'Changing target from: {self.target_title}')
            print(f'to: {target_title}')
        self.target_title = target_title
        self.target_text = WikipediaText(self.pages[self.target_title])

    def save(self):
        with open(self.storage_file, 'wb') as f:
            pickle.dump((self.wiki, self.terms, self.pages), f)
