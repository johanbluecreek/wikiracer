# WikiRacer

A silly little automated [Wikipedia-racer](https://en.wikipedia.org/wiki/Wikipedia:Wiki_Game), in which the goal is to find the shortest path between two selected articles.

## Usage

To try a search between two articles, change the two lines for `start_title` and `target_title` in `main.py`,

```
def main():

    start_title = [YOUR START TITLE HERE]
    target_title = [YOUR TARGET TITLE HERE]
    simple_search(start_title, target_title)
```

then run `main.py`

```
$ ./main.py
```

There is a lot of informative prints during the search, and at the end there will be three paths printed. The first one printed will be the actual path found, and the other two by using some optimisations.

The first time this is run, it will have to do some "training", which will take some time (20 minutes on my computer and lousy internet connection). This only happens the first time, after that the "training" is stored on file.

## Design

### Designing a distance measure (or fitness)

The idea was to compare two articles via a form of pair-wise fitness: The Term-Frequency (TF) of the target, here represented by $`v_t`$ for the term $`t`$ ($`t`$ here represents all terms present in target only), and the TF of a current article $`w_t`$ should have a fitness

```math
\text{fitness} \sim (v_t - w_t)
```

which of course would be fairly bad, as text frequencies of common words would dominate the fitness. Hence a weight of [Inverse Document Frequency (IDF)](https://en.wikipedia.org/wiki/Tf%E2%80%93idf) is applied, where the bare IDF for a term would be given by

```math
I_t = \log \frac{D}{n_t}
```

where $`n_t`$ is the number of documents in which the term $`t`$ occurs, and $`D`$ is the total number of documents. Hence the more common a term is, the closer to `1` the ratio is, hence the close to zero the weight is, meaning

```math
\text{fitness} \sim (v_t - w_t) I_t
```

is a more suitable fitness, accounting for the relevance of terms.

Note that a "term" is not limited to mean a single word, but instead an [N-gram model](https://en.wikipedia.org/wiki/N-gram) is applied, storing sequences of words. Higher degree of the N-gram contains more context, and may hence be more relevant. E.g. in something like "University of Wisconsin", as words "University" and "Wisconsin", may be relevant to the target (and the "of" having a low weight due to the IDF), the full degree-3 N-gram "University of Wisconsin" contains more context than the words alone.

We can apply a weight to the fitness depending on the N-gram degree of the term, applying a weight $`W_{D(t)}`$ for a term $`t`$ of degree $`D`$,

```math
\text{fitness} \sim abs(v_t - w_t) I_t W_{D(t)}
```

One may also want to tune the TF to the ratio of lengths of the target and current article, as a current article may solve differences in TF by simply being longer instead of more relevant, so the ratio of the target and current article (in number of words) $`l = L_t/L_c`$ may play in and we can apply it

```math
\text{fitness} \sim abs(v_t - l w_t) I_t W_{D(t)}
```

and lastly an over-count of frequencies may be preferable over an under-count of a term, as the presence of a context, even if over-counted, is more relevant than the lack of one. We can also add a contribution for this

```math
\text{fitness} \sim abs(v_t - l w_t) I_t W_{D(t)} (1-(1-w_{-})\delta_{-1,sgn(v_t - l w_t)})
```

We can also scatter some additional tunable powers on there to adjust the relevance of each factor of the fitness

```math
\text{fitness} \sim (abs(v_t - l^{l_p} w_t) (1-(1-w_{-})\delta_{-1,sgn(v_t - l^{l_p} w_t)}))^tp I_t^{i_p} W_{D(t)}
```

Still this is not enough. As we start to "train", meaning the IDF's gets refined through new articles being added, rare terms will have and ever growing IDF (even though the logarithm grows slowly). Instead we are looking for it to be asymptotic; common terms having a weight of close to zero, rare terms having a weight close to one. One way to achieve this is to

```math
I_t \to \tanh (I_t) = 2\frac{D^2}{D^2 + n_t^2} - 1 =: E_t
```

which for $`n_t \leq D`$ is in `[0,1]`. Making the fitness:

```math
\text{fitness} \sim (abs(v_t - l^{l_p} w_t) (1-(1-w_{-})\delta_{-1,sgn(v_t - l^{l_p} w_t)}))^tp E_t^{i_p} W_{D(t)}
```

There are hence the following tunable meta-parameters that heavily affects the fitness

  * $`l_p`$: setting the relevance of scaling the frequencies by relative article length (default = `1.0`, feature is turned off if set to `0.0`)
  * $`i_p`$: scaling the relevance of the "IDF" weights, higher numbers scales down the relevance of irrelevant terms, `0.0` would turn off using IDF weights.
  * $`t_p`$: scaling the relevance of the TF factor.
  * $`W_{d}`$, one weight for each degree.
  * $`w_{-}`$ weight when over-counting terms. If between `[0.0, 1.0]`, over-counting costs less than under-counting.

And that is essentially the pair-wise fitness of a current article w.r.t. the desired target.

### Training

To establish an IDF for each term, we need to collect a number of documents, which means in a way we are "training" the IDF weights.

To measure how well trained our model is, we can compare the fitness between the target article and the start article, add, say, 1000 random articles (the default), and then compare the fitness again. If the fitness have shifted by more than 1% (default), another 10 articles are added.

The IDF weights will also shift during a search, and may shift the relevance of terms. Meaning, as we get closer to the target, the articles will have more relevant context terms, hence IDF weights will get lower, making those terms less relevant. It is generally not necessarily a danger, as the still missing terms between current and target will have high IDF, meaning it refines the search as it goes. It may however cause shifts in the fitness, hence after each iteration, we compare if the current computed fitness between start and target, has shifted by more than 1% compared to the last iteration, and if so add 10 random articles until the fitness stabilize again.

A general problem is that with training, hence with each iteration, the fitness does increase. Meaning that links considered early usually have a slightly lower fitness.

### Iterative procedure

Given a start and target, the algorithm selects start as current article to consider, and asks for the links of the article (using the [Wikipedia-API python module](https://github.com/martin-majlis/Wikipedia-API/)). The links are sorted according to a fitness of the current title and target text, as comparing two articles takes a long time. The best suited links are considered first, and once a number of better articles (that is, with lower fitness than the previous article) has been found, the next iteration proceeds with the best of those articles collected. If no better has been found, the algorithm selects the best in the current article.

The target article may also be replaced with a more popular article, one link away from the actual target, just to make it easier to find. The search is then split in two: First finding replacement target, then finding the actual target.

Once a path has been found, it will truncate the to the shortest path that exists in the trace of the search. It will also go through all links found in the trace to see if a shorter path can be found. These optimisations in the path is not due to analysing the text, and is cheating a bit.

## Some statistics

In an attempt to generate statistics, 100 runs were made with articles selected from `Special:Random` (start and target). Out of the 100, 69 finished in the set time and iteration limit (2h and 2000 iterations, respectively), meaning 31 failures. The total time for the test was 76h 14m 16.7s (most of which spent waiting on Wikipedia-API requesting and processing pages, and distributed over 8 simultaneous runs), out of which 63.2% (48h 12m 25.4s) was spent on the failed searches.

Out of the 31 failures, 3 failed due to the article being isolated (having no non-special articles linking to it), 21 failed due to time-out, 1 due to iteration limit, and the rest for yet-to-be-explained reasons.

Here are some tabulated results of the successes:

  | stat              | min    | max          | avg       | median    |
  |------             |-----   |-----         |-----      | ------    |
  | time              | 31.6s  | 1h 54m 54.1s | 24m 22.5s | 13m 30.0s |
  | path length found | 4      | 1532         | 137.8     | 46        |
  | path minus loops  | 4      | 108          | 20.1      | 15        |
  | path optimized    | 4      | 33           | 12.1      | 10        |

Regarding "time" and "path length found" we see that average is much lower than max, and median much lower than average, which seems to imply that searches that take a long time or experience lots of loops are outliers.

We can also see that the presence of loops is what increases the iterations by quite a lot, hence there should be some optimizations to be made here. This should probably be improved by how loops are handled.

There is also some room for improvement in finding the optimized path. This probably has most to do with how the fitness is computed, which may also have an effect on the presence of loops.

Regarding the minimum for the "path" entries in the table, it is the same solution (not, however, in time) for all three rows, and one may ask if this -- finding the optimized path -- is an outlier. Our of the 100 runs, 10 found the optimized path, one where the 4 steps in the optimized path, five with 5 steps, thee with 6 steps, and a single one with eight steps.

Of all searches 41 used the "cheat" (where a simpler target was is chosen as a first step), 29 in the successes (42%) and 12 in the failures (39%). It still needs to be determined if the cheat was the reason for these failures. Of the 10 solutions where the optimized path was found 7 was using the "cheat".
