#!/usr/bin/env python3

from tqdm import tqdm
import random, time, os, sys, subprocess, pathlib

from wikiracer.wikiracer import WikipediaRacer, WikipediaText

def make_background(wr, target_title, start_title, old_fit = None):
    if old_fit is None:
        print('Establishing background.')
        old_fit = wr.fitness(target_title, start_title)
        wr.print_top(target_title)
        print(f'old: {old_fit}')
        if len(wr) < 100:
            wr.add_random_pages(1000, safe=True)
            wr.save()
        else:
            wr.add_random_pages()
    current_fit = wr.fitness(target_title, start_title)
    diff = abs(old_fit - current_fit)/current_fit
    print(f'diff: {diff}')
    print(f'new: {current_fit}')
    old_fit = current_fit
    while diff > 0.001:
        wr.print_top(target_title)
        wr.add_random_pages()
        current_fit = wr.fitness(target_title, start_title)
        diff = abs(old_fit - current_fit)/current_fit
        print(f'diff: {diff}')
        print(f'new: {current_fit}')
        old_fit = current_fit
    print('Background complete.')

def truncate_trace_loops(trace):
    new_trace = []

    i = 0
    while i < len(trace):
        idxs = [ j for j in range(len(trace)) if trace[j][0] == trace[i][0]]
        new_trace.append(trace[i])
        if len(idxs) > 1:
            i = idxs[-1]+1
        else:
            i += 1

    return new_trace

def truncate_trace_links(wr, trace):
    new_trace = []

    i = 0
    while i < len(trace):
        idxs = [ t for t in range(len(trace)) if t > i and trace[t][0] in wr.get_page(trace[i][0]).links.keys() ]
        new_trace.append(trace[i])
        if len(idxs) > 1:
            i = idxs[-1]
        else:
            i += 1

    return new_trace

def search(wr, target_title, start_title, trace=None, kill_time=float('inf'), kill_iters=float('inf')):
    best_fit = wr.fitness(target_title, start_title)
    print(f'Starting fitness: {best_fit}')
    current_title = start_title
    if trace is None:
        trace = [(current_title, best_fit)]
    if best_fit == 0.0:
        print('Articles the same, added to trace an exiting.')
        # this may cause unnecessary steps if it is an article with multiple titles
        trace.append((target_title, best_fit))
        return trace

    dead_ends = []

    saved_fit = best_fit

    timer = time.time()
    counter = 0

    done = False
    while not done:
        if time.time() - timer > kill_time:
            print('Max-time exceeded, stopping.')
            return trace
        if counter >= kill_iters:
            print('Max-iterations exceeded, stopping.')
            return trace
        else:
            counter += 1
        wr.print_top(target_title)
        # Sort titles by fitness, make lower term degrees more important, and make
        # the IDF more pronounced
        # This sorting function takes some 1-10 seconds. Could perhaps be improved?
        links = wr.get_link_title_list(current_title, fn=lambda x: wr.fitness(target_title, WikipediaText(x.lower()), dw=(1.0, 100.0, 10.0), ip=5, lp=0) + random.random()*best_fit*0.01)
        if target_title in links:
            trace.append((target_title, 0.0))
            print('\nEnd-point found!')
            print('This was the path: ')
            for t in trace:
                print(f'  {t}')
            print(f'That is a total of {len(trace)} links (start and target included).\n')
            break
        old_links, _ = zip(*trace)
        forbidden_nexts = []
        if current_title in old_links[:-1]:
            # if we encounter a loop (current_title is in the trace), then we need to
            # prevent the loop by removing the next step in the loop.
            forbidden_nexts = [ old_links[i+1] for i in range(len(old_links)-1) if old_links[i] == current_title ]
            print('\nStart of loop found, removing the following links:')
            for link in forbidden_nexts:
                print(f'  {link}')
            print()
        links = [ link for link in links if link.lower() != current_title.lower() and link not in forbidden_nexts and link not in dead_ends ]
        # if all links happen to be in forbidden_nexts, we need to add this page as a dead-end,
        # and go back one step
        if len(links) == 0 or len(forbidden_nexts) > 10:
            print('\nDead-end found.')
            dead_ends.append(current_title)
            idx = [ title for title,fit in trace ].index(current_title) - 1
            current_title, current_fit = trace[idx]
            print(f'Stepping back to: {current_title} ({current_fit}).\n')
            continue
        current_best = ('',float('inf'))
        betters = 0
        for i in tqdm(range(min(len(links),20)), file=sys.__stdout__):
            current_fit = wr.fitness(target_title, links[i])
            if current_fit < current_best[1]:
                current_best = (links[i], current_fit)
                print(f' Best found: "{links[i]}" ({current_fit})')
            if current_fit < best_fit:
                print(f'  Found better than previous best: "{links[i]}" ({current_fit})')
                betters += 1
                if betters > 1 or (best_fit - current_fit)/best_fit > 0.1:
                    print('  Found enough better articles or a great improvement, breaking.')
                    break
            if current_fit == 0.0:
                break
        current_title = current_best[0]
        if current_best[1] < best_fit:
            best_fit = current_best[1]
        trace.append(current_best)
        print(f'\nSelecting link: "{current_title}" ({current_best[1]})\n')
        if len(trace) % 5 == 0:
            print(f'Current trace is ({len(trace)}):')
            if len(trace) <= 15:
                for t in trace:
                    print(f'  {t}')
            else:
                for t in trace[:5]:
                    print(f'  {t}')
                print('  ...')
                for t in trace[-10:]:
                    print(f'  {t}')
            print(f'Trace has length: {len(trace)}, and target is: {target_title}')
            print()
        if current_best[1] == 0.0:
            print('\nEnd-point found!')
            print('This was the path: ')
            for t in trace:
                print(f'  {t}')
            print(f'That is a total of {len(trace)} links (start and target included).\n')
            break
        print('Making sure background has not shifted too much:')
        make_background(wr, target_title, start_title, saved_fit)
        saved_fit = wr.fitness(target_title, start_title)

    return trace

def simple_search(start_title, target_title, kill_time=float('inf'), kill_iters=float('inf')):
    wr = WikipediaRacer()
    ret = [len(wr)]

    print(f'Current document number: {len(wr)}')

    start_time = time.time()

    if len(wr.get_backlink_title_list(target_title)) == 0:
        print('Isolated article, no path to it.')
        return [len(wr), 0, 0, 0, 0.0, False, False]

    cheat = False
    if len(wr.get_backlink_title_list(target_title)) < 10:
        print(f'The target has few pages linking to it ({len(wr.get_backlink_title_list(target_title))}), so we will be setting a simpler target.')
        cheat = True

    if cheat:
        # If the target has very few articles that links into it, we select the article
        # that links to the actual target had it self has the most backlinks as the first
        # target, because it is easier to find.
        # This is cheating, because the algorithm does not play after the same rules as
        # a human player.
        kill_time = kill_time/2
        kill_iters = kill_iters/2
        new_target_title = [ l for l in wr.get_backlink_title_list(target_title, sort=True) if l != target_title ][0]
        wr.set_target(new_target_title)
        make_background(wr, new_target_title, start_title)
        trace = search(wr, new_target_title, start_title, kill_time=kill_time, kill_iters=kill_iters)
        wr.set_target(target_title)
        new_start = trace[-1][0]
        trace = search(wr, target_title, new_start, trace, kill_time=kill_time, kill_iters=2*kill_iters-len(trace))
    else:
        wr.set_target(target_title)
        make_background(wr, target_title, start_title)
        trace = search(wr, target_title, start_title, kill_time=kill_time, kill_iters=kill_iters)

    ret.append(len(trace))

    trace_titles, _ = zip(*trace)
    if len(trace_titles) != len(set(trace_titles)):
        print('Loops found, contracting the trace to shortest path...')
        trace = truncate_trace_loops(trace)
        print('\nThis is the new path: ')
        for t in trace:
            print(f'  {t}')
        print(f'That is a total of {len(trace)} links (start and target included).\n')

    ret.append(len(trace))

    print('Going over the trace to find optimal path...')
    trace = truncate_trace_links(wr, trace)
    print('\nThis is the new path: ')
    for t in trace:
        print(f'  {t}')
    print(f'That is a total of {len(trace)} links (start and target included).\n')

    ret.append(len(trace))

    print(f'Search is complete, it took: {time.time() - start_time} seconds')

    ret.append(time.time() - start_time)
    ret.append(cheat)

    ret.append(target_title == trace[-1][0] or trace[-1][1] == 0.0)

    return ret

def gen_statistics_titles(num=100):
    for _ in tqdm(range(num)):
        wr = WikipediaRacer()

        start_title = wr.random_title(safe=True)
        target_title = wr.random_title(safe=True)

        stats_titles = os.environ['HOME'] + '/.wikiracer/stats_titles'
        with open(stats_titles, 'a') as file:
            file.write(f'("{start_title}", "{target_title}")\n')

def get_running_instances():
    ps = subprocess.Popen(('ps', '-ef'), stdout=subprocess.PIPE)
    ps = subprocess.Popen(('grep', '#wikiracer_running'), stdin=ps.stdout, stdout=subprocess.PIPE)
    ps = subprocess.check_output(('grep', '-v', 'grep'), stdin=ps.stdout)
    return len([ line for line in ps.split(b'\n') if b'#wikiracer_running' in line])

def generating_statistics(num=100):
    """
    Make sure to run main.py in bash with the comment '#wikiracer_running'
    """

    time.sleep(random.random()*10)

    s = 'success; start; target; background size; trace length; trace length loops removed; trace length optimized; time (s); cheat\n'

    log_file = os.environ['HOME'] + '/.wikiracer/stats.log'
    if not os.path.isfile(log_file):
        with open(log_file, 'w') as file:
            file.write(s)

    kill_time = 2*60*60.0
    kill_iters = 2000

    while True:

        titles = get_name_for_statistics()
        if titles is None:
            break

        start_title, target_title = titles

        w,t1,t2,t3,t,c,f = simple_search(start_title, target_title, kill_time=kill_time, kill_iters=kill_iters)

        s = f'{f}; {start_title}; {target_title}; {w}; {t1}; {t2}; {t3}; {t}; {c}\n'

        with open(log_file, 'a') as file:
            file.write(s)

def get_name_for_statistics():
    stats_title_files = os.environ['HOME'] + '/.wikiracer/stats_titles'
    with open(stats_title_files, 'r') as file:
        stats_titles = list(map(eval, file.readlines()))

    taken = os.environ['HOME'] + '/.wikiracer/taken'
    if not os.path.isfile(taken):
        pathlib.Path(taken).touch()
    with open(taken, 'r') as file:
        taken_titles = list(map(eval, file.readlines()))

    processed_titles = [
        titles
    for titles in stats_titles if titles not in taken_titles ]

    if len(processed_titles) > 0:
        titles = processed_titles[0]
        with open(taken, 'a') as file:
            file.write(f'("{titles[0]}", "{titles[1]}")\n')
        return processed_titles[0]
    else:
        if get_running_instances() == 1:
            os.remove(taken)
        return None

def main():

    start_title = 'Python (programming language)'
    target_title = 'James Hoban'
    simple_search(start_title, target_title)
    #generating_statistics()

if __name__ == '__main__':
    main()
